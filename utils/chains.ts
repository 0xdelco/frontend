import type { Chain } from '@rainbow-me/rainbowkit'
export const opChain: Chain & {
  unsupported?: boolean | undefined
} = {
  id: 10,
  name: 'Optimism',
  network: 'Fantom',
  iconUrl:
    'https://assets.coingecko.com/coins/images/25244/small/Optimism.png?1660904599',
  iconBackground: '#fff',
  nativeCurrency: {
    decimals: 18,
    name: 'Optimism',
    symbol: 'ETH',
  },
  rpcUrls: {
    default: 'https://mainnet.optimism.io	',
  },
  blockExplorers: {
    default: { name: 'Optimism', url: 'https://optimistic.etherscan.io' },
  },
  testnet: false,
  unsupported: false,
}
