import type { Chain } from '@rainbow-me/rainbowkit'

export type Token = {
  title?: string
  description?: string
  images?: React.ReactElement[]
  chain?: Chain & {
    unsupported?: boolean | undefined
  }
  contract?: string
  decimals?: number
  balance?: number
}
