const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    tablet: '640px',
    laptop: '1024px',
    desktop: '1024px',

    extend: {
      space: {
        '0px': '0px',
        '64px': '0px',
      },
      fontFamily: {
        roboto: ['Roboto Mono', 'monospace'],
        'roboto-cond': ['Roboto Condensed', 'cursive'],
        transformers: ['Transformers Movie'],
      },
      boxShadow: {
        internal:
          'inset 6px 2px 4px rgba(0, 0, 0, 0.32), inset 6px 2px 4px rgba(0, 0, 0, 0.32), inset 6px 2px 4px rgba(0, 0, 0, 0.32), inset -0px -2px 1px rgba(255, 255, 255, 0.08)',
        internal2:
          'inset 2px 2px 4px rgba(0, 0, 0, 0.32), inset -2px -2px 1px rgba(255, 255, 255, 0.08)',
      },
      backgroundImage: {
        //wave: "url('/wave.png')",
        //electro: "url('/electro.svg')",
        //robot: "url('/robot-light.png')",
        //circuit: "url('/circuit.jpeg')",
        //robots: "url('/robots.png')",
        blackwhite: "url('/banner1.png')",
      },
    },
  },
  plugins: [],
};
