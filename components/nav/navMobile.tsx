import Link from 'next/link';
import { useContext } from 'react';
import { StateContext } from '../../context/StateContext';
import { Page } from '../../utils/enums';
import { nav } from '../../utils/constants';
import Navbar from './navbar';

export default function NavMobile() {
  const { page, readyToMint, setPage } = useContext(StateContext);

  return (
    <nav className="flex xl:hidden flex-row justify-center items-center fixed space-x-4 bottom-0 bg-slate-900 w-full h-20 z-50">
      <Navbar
        styleFirstLink="text-white text-xs sm:text-xl font-roboto-cond hover:opacity-75"
        styleFirstLinkTwo="text-slate-500 border-b-2 border-slate-500"
        styleSecondLink="flex justify-center items-center text-white bg-blue-900 py-2 px-2 xl:text-lg font-roboto-cond  shadow-xl rounded-md transition ease-in-out hover:scale-[1.03]"
      />
    </nav>
  );
}
