import Link from 'next/link';
import Image from 'next/image';
import { ReactElement, useContext, useEffect } from 'react';
import Head from 'next/head';
import Nav from './nav/nav';
import NavMobile from './nav/navMobile';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ConnectButton } from '@rainbow-me/rainbowkit';
import Twitter from './svg/twitter';
import Discord from './svg/discord';
import Telegram from './svg/telegram';

interface layout {
  children: ReactElement;
}

export default function Layout({ children }: layout) {
  useEffect(() => {
    document.documentElement.lang = 'en-us';
    document.documentElement?.classList.add('scroll-smooth');
  }, []);
  return (
    <div className="flex flex-col justify-between bg-[url('/banner/banner12_3.jpg')] bg-no-repeat bg-cover font-roboto-cond relative  scroll-smooth min-h-screen">
      <Head>
        <title>OPTIMISM PRIME</title>
        <meta name="description" content="Token on Optimism" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />

        <link rel="icon" href="/logo_round.png" />
      </Head>

      {/* NAV DESKTOP */}
      <Nav />

      {/* LOGO MOBILE */}
      <div className="flex flex-col xl:hidden z-20">
        <Link href="/">
          <a>
            <div className="flex flex-row justify-center items-center font-transformers text-white text-3xl lg:text-5xl mt-2 mb-2">
              <Image
                src="/logo_round.png"
                alt="logo"
                width={96}
                height={96}
                unoptimized={true}
                quality={100}
              />
              <div className="mt-2">
                <span className="italic text-[#1c1c1c]/80 mr-[5px]">
                  OPTIMISM
                </span>
                <span>PRIME</span>
              </div>
            </div>
          </a>
        </Link>
        <div className="flex self-center pb-3">
          <ConnectButton accountStatus="address" label="CONNECT" />
        </div>
        <div className="flex xl:hidden flex-row justify-center items-center space-x-5 mb-8 bg-slate-900/40 rounded-full w-44 py-2 self-center">
          <Link href="https://twitter.com/Optimism_Pr">
            <a
              target="blank"
              className="w-8 hover:opacity-70 scale-75 hover:scale-100"
            >
              <Twitter />
            </a>
          </Link>
          <Link href="https://discord.gg/SpfdM2VwUy">
            <a
              target="blank"
              className="w-8 hover:opacity-70 scale-75 hover:scale-100"
            >
              <Discord />
            </a>
          </Link>
          <Link href="https://t.me/OptimismPrimePortal">
            <a
              target="blank"
              className="w-8 hover:opacity-70 scale-75 hover:scale-100"
            >
              <Telegram />
            </a>
          </Link>
        </div>
      </div>
      <ToastContainer
        containerId="toast-notification"
        position="top-right"
        autoClose={6000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick={false}
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />

      <main className="flex items-center flex-col md:px-12 font-roboto-cond justify-between h-full">
        <>{children}</>
      </main>

      {/* FOOTER */}
      <footer
        className={`
          flex flex-col
          h-10
          pb-[79px] xl:pb-0
          lg:justify-end
        `}
      >
        <div className="flex justify-center bg-black h-10 text-red-900">
          <Link href="http://www.freepik.com">
            <a className="flex items-center">Background: designed by Freepik.</a>
          </Link>
        </div>
        {/* NAV MOBILE */}
        <NavMobile />
      </footer>
    </div>
  );
}
