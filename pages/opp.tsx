import Link from 'next/link';
import Image from 'next/image';

export default function Opp() {
  return (
    <div className="opp-page flex flex-col max-w-[1920px] flex-1   h-full py-0 px-12">
      {/* TEXT */}
      <div className="flex flex-col text-white space-y-5 z-30 text-justify bg-black bg-opacity-80 px-10 pb-10 shadow-[1px_5px_30px_rgba(255,255,255)]">
        <p className="text-3xl md:pt-6 leading-relaxed">
          <span className="font-transformers text-4xl text-white/80">
            OptiPrimus
          </span>
          , the creator god - the multiversal force for good has provided some
          of his life force to bring Optimism Prime to life.
        </p>
        <div className="text-2xl  space-y-12">
          <p>
            Even in the face of losing decentralization to the combined might of
            the Regulator forces, Optimism Prime never abandoned his fight to
            liberate the Optimistic Chain. Given the current events and
            regulations, Optimism Prime and the decentrabots have staged a plan
            to counter this unfair legislators: the{' '}
            <Link href="https://optimistic.etherscan.io/token/0x676f784d19c7f1ac6c6beaeaac78b02a73427852">
              <a target="blank">$OPP</a>
            </Link>{' '}
            token.
          </p>

          <p>
            Optimism Prime (
            <Link href="https://optimistic.etherscan.io/token/0x676f784d19c7f1ac6c6beaeaac78b02a73427852">
              <a target="blank">$OPP</a>
            </Link>
            ) is the first <span className="underline">FAIR LAUNCH</span> token
            mechanics in the Optimism Chain. NO PRESALE, NO VC's. Just a fair
            token, cheap gas and lots of fun!
          </p>
          <p className="z-40">
            Token distribution: 90%{' '}
            <Link href="https://optimistic.etherscan.io/token/0x676f784d19c7f1ac6c6beaeaac78b02a73427852">
              <a target="blank">$OPP</a>
            </Link>{' '}
            is for liquidity providing on
            <Link href="https://app.velodrome.finance/swap">
              <a
                target="blank"
                className="mx-1 text-[#a52a2a] hover:text-red underline"
              >
                Velodrome
              </a>
            </Link>{' '}
            and 10% will be kept as treasury to keep the project development.
          </p>
          <p>
            We have suffered losses, but we've not lost the war, we can still
            have lots of fun, and most important, we can still do it FREELY on
            Optimism.
          </p>
          <p>
            Hop on our journey and join our socials. Be a part of the first
            legit meme token and let's save the blockchain together.{' '}
            <span className="font-transformers text-white/80">
              Now all we need is a little Energon and a lot of Luck.
            </span>
          </p>
        </div>
      </div>
      <div className="flex flex-col max-w-[1920px] items-center font-transformers h-[23.2rem] justify-center lg:mx-0 xl:mx-0 xl:space-x-[0px] lg:space-x-[0px]">
        <div className="hidden lg:flex opacity-80  hover:scale-105 transition ease-in-out mx-14">
          <Image src="/logo_round.png" alt="logo" width={200} height={200} />
        </div>
      </div>
    </div>
  );
}
