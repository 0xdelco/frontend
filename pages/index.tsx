import type { NextPage } from 'next';
import { useCallback, useEffect, useState, useContext } from 'react';
import { getPairInformationByChain } from 'dexscreener-api';
import { StateContext } from '../context/StateContext';

const OPP_ETH_DEXSCREENER_ADDRESS =
  '0x9e0fed4f8284b5b81601b4c7fa50f68dbf958a86';

const Round = ({
  text,
  children,
}: {
  text: string;
  children?: React.ReactNode;
}) => {
  return (
    <div className="flex flex-col justify-center items-center">
      <span className="text-black">{text}</span>
      {children}
    </div>
  );
};

const roundStyles = () => {
  return 'flex flex-col justify-center items-center hover:scale-105 transition ease-in-out gap-6 p-6 bg-[#bb292d] w-[300px] h-[300px] sm:w-[400px] sm:h-[400px] 2xl:w-[400px] 2xl:h-[400px] rounded-full shadow-[0_1px_135px_rgba(255,255,255)]';
};

const Home: NextPage = () => {
  const { isLoading, setIsLoading } = useContext(StateContext);
  const [stats, setStats] = useState<any>(null);
  const getStats = useCallback(async () => {
    setIsLoading(true);
    const pairsResponse = await getPairInformationByChain(
      'optimism',
      OPP_ETH_DEXSCREENER_ADDRESS as string
    );

    setStats(pairsResponse.pair);
    setIsLoading(false);
  }, [setStats, setIsLoading]);

  useEffect(() => {
    getStats();
  }, [getStats]);

  return (
    <div className="home-page w-full flex justify-center">
      <div className="flex justify-evenly items-center h-full w-full max-w-[1920px] 2xl:min-h-[100%]  rounded-3xl p-6 py-[3.8rem] font-transformers bg-transparent text-white xl:mt-8 flex-wrap text-3xl">
        <div className={roundStyles()}>
          <Round text="MKT CAP">
            <p className="xl:text-5xl">
              {!stats || isLoading
                ? '---'
                : `${stats?.fdv?.toLocaleString('en-us')}`}
            </p>
          </Round>
          <Round text="VOLUME">
            <p className="xl:text-5xl">
              {!stats || isLoading
                ? '---'
                : `${stats?.volume?.h24?.toLocaleString('en-us')}`}
            </p>
          </Round>
        </div>

        <div className={roundStyles()}>
          <Round text="USD PRICE">
            <p className="xl:text-5xl">
              {!stats || isLoading
                ? '---'
                : `${stats?.priceUsd?.toLocaleString('en-us')}`}
            </p>
          </Round>
          <Round text="LOCKED VELO">
            <p className="xl:text-5xl">
              {Number(460289.53).toLocaleString('en-us', {
                maximumFractionDigits: 2,
              })}
            </p>
          </Round>
        </div>
      </div>
    </div>
  );
};

export default Home;
