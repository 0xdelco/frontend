import Image from 'next/image';
import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { opChain } from '../utils/chains';
import '@rainbow-me/rainbowkit/styles.css';
import { createClient, configureChains, WagmiConfig } from 'wagmi';
import { jsonRpcProvider } from 'wagmi/providers/jsonRpc';
import StateProvider from '../context/StateContext';

import {
  connectorsForWallets,
  RainbowKitProvider,
  AvatarComponent,
  wallet,
  Chain,
  darkTheme,
} from '@rainbow-me/rainbowkit';
import Layout from '../components/layout';

const { chains, provider } = configureChains(
  [opChain as Chain],
  [jsonRpcProvider({ rpc: (chain) => ({ http: chain.rpcUrls.default }) })]
);

const connectors = connectorsForWallets([
  {
    groupName: 'Recommended',
    wallets: [
      wallet.walletConnect({ chains }),
      wallet.metaMask({ chains }),
      wallet.ledger({ chains }),
      wallet.trust({ chains }),
      wallet.coinbase({ appName: 'optimismprie.io', chains }),
    ],
  },
]);

const wagmiClient = createClient({
  autoConnect: true,
  connectors,
  provider,
});

const CustomAvatar: AvatarComponent = () => {
  return (
    <div className="flex bg-skin-base/30 p-3 rounded-full">
      <Image src="/logo_round.png" width={80} height={80} alt="wallet logo" />
    </div>
  );
};

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <WagmiConfig client={wagmiClient}>
      <RainbowKitProvider
        chains={chains}
        avatar={CustomAvatar}
        theme={darkTheme()}
      >
        <StateProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </StateProvider>
      </RainbowKitProvider>
    </WagmiConfig>
  );
}

export default MyApp;
