import { ethers } from 'ethers'
import {
  STAKING_ABI,
  STAKE_SEASONS,
  SEASON_TO_REWARDS_PER_DAY,
  TOKENS,
} from '../utils/constants'
import { getBalance } from './shared/balance'
import checkApproval from './shared/check-approval'
import notificateTransaction from './shared/notificate-transaction'
import handleError from '../utils/handleErrors'

const opp = TOKENS[0]

export async function info(season: number, signer?: ethers.Signer) {
  const provider = signer?.provider

  const contract = new ethers.Contract(
    STAKE_SEASONS[season - 1]?.contract as string,
    STAKING_ABI,
    provider,
  )

  try {
    const endStakingDate = await contract.END_STAKING_UNIX_TIME()
    const lockedUntil =
      Number(endStakingDate) === 0 ? 1669126875 : endStakingDate

    const totalStaked = await contract.totalStaked()
    const maxStakedPermited = await contract.MAX_OPP_STAKED()
    const tvl = Number(ethers.utils.formatEther(totalStaked.toString()))
    const apr =
      tvl === 0 ? 0 : (SEASON_TO_REWARDS_PER_DAY[season - 1] / tvl) * 365 * 100

    const response = {
      lockedUntil: Number(lockedUntil),
      totalStaked: Number(ethers.utils.formatEther(totalStaked.toString())),
      apr: Number(apr),
      maxStakedPermited: Number(
        ethers.utils.formatEther(maxStakedPermited.toString()),
      ),
    }

    return response
  } catch (e) {
    console.log(e)
  }
}

export async function userInfo(season: number, signer: ethers.Signer) {
  let balance, rewards, details
  const contract = new ethers.Contract(
    STAKE_SEASONS[season - 1]?.contract as string,
    STAKING_ABI,
    signer,
  )

  try {
    const account = await signer.getAddress()
    balance = await getBalance(opp, signer)
    rewards = await contract.calculateReward(account)
    details = await contract.stakingDetails(account)

    return {
      balance: balance ? balance : 0,
      userStakeBalance: ethers.utils.formatEther(
        details ? details?.stakingBalance.toString() : '0',
      ),
      yield: ethers.utils.formatEther(rewards ? rewards?.toString() : '0'),
    }
  } catch (e) {
    console.log(e)
  }
}

export async function stake(
  season: number,
  signer: ethers.Signer,
  amount: string,
) {
  const network = await signer.provider?.getNetwork()
  const stakeContract = STAKE_SEASONS[season - 1]

  try {
    const approved = await checkApproval(opp, stakeContract, signer)

    if (approved) {
      const contract = new ethers.Contract(
        stakeContract?.contract as string,
        STAKING_ABI,
        signer,
      )
      const tx = await contract.stake(
        ethers.utils.parseEther(amount.toString()),
      )
      await notificateTransaction(
        network as ethers.providers.Network,
        tx,
        'Stake',
      )
    }
  } catch (e) {
    handleError({ e: e, notificate: true })
  }
}

export async function unstake(season: number, signer: ethers.Signer) {
  const network = await signer.provider?.getNetwork()
  const stakeContract = STAKE_SEASONS[season - 1]

  try {
    const approved = await checkApproval(opp, stakeContract, signer)
    if (approved) {
      const contract = new ethers.Contract(
        stakeContract?.contract as string,
        STAKING_ABI,
        signer,
      )

      const tx = await contract.withdrawStaked()
      await notificateTransaction(
        network as ethers.providers.Network,
        tx,
        'Unstake',
      )
    }
  } catch (e) {
    handleError({ e: e, notificate: true })
  }
}

export async function claim(season: number, signer: ethers.Signer) {
  const network = await signer.provider?.getNetwork()
  const stakeContract = STAKE_SEASONS[season - 1]

  try {
    const approved = await checkApproval(opp, stakeContract, signer)
    if (approved) {
      const contract = new ethers.Contract(
        stakeContract?.contract as string,
        STAKING_ABI,
        signer,
      )

      const tx = await contract.claimRewards()
      await notificateTransaction(
        network as ethers.providers.Network,
        tx,
        'Claim',
      )
    }
  } catch (e) {
    handleError({ e: e, notificate: true })
  }
}
