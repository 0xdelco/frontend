import { ethers } from 'ethers';
import { opChain } from '../../utils/chains';
import { TOKEN_ABI } from '../../utils/constants';
import Notification from '../../components/Notification';
import handleError from '../../utils/handleErrors';
import type { Token } from '../../utils/types';

export async function giveApproval({
  owner,
  spender,
  signer,
  customAbi,
}: {
  owner: Token;
  spender: Token;
  signer: ethers.Signer;
  customAbi?: any;
}) {
  try {
    const contract = new ethers.Contract(
      owner.contract as string,
      customAbi ? customAbi : TOKEN_ABI,
      signer
    );

    const tx = await contract.approve(
      spender.contract as string,
      ethers.utils.parseUnits(
        '1000000000000000000000000000000000000000000000000000',
        owner.decimals
      )
    );
    const txUrl = opChain.blockExplorers?.default.url + tx.hash.toString();

    Notification({
      type: '',
      title: 'Transaction Submitted',
      message: 'Your approval transaction was successfully submitted.',
      link: txUrl,
    });

    const receipt = await tx.wait();
    Notification({
      type: 'success',
      title: 'Transaction Confirmed',
      message: `Approval confirmed in block ${receipt.blockNumber}`,
      link: txUrl,
    });

    return receipt;
  } catch (e) {
    return handleError({ e: e, notificate: true });
  }
}
