import { ethers } from 'ethers'
import { getAllowance } from './allowance'
import { giveApproval } from './approval'
import delay from '../../utils/delay'
import type { Token } from '../../utils/types'

export default async function checkApproval(
  owner: Token,
  spender: Token,
  signer: ethers.Signer,
  customAbi?: any,
) {
  let allowed = await getAllowance({
    owner: owner.contract as string,
    spender: spender.contract as string,
    signer,
    customAbi: customAbi ? customAbi : null,
  })

  if (!allowed) {
    const approval = await giveApproval({
      owner: owner,
      spender: spender,
      signer,
      customAbi: customAbi ? customAbi : null,
    })

    if (approval === 'ACTION_REJECTED' || approval === '-32603') return false
  }

  while (!allowed) {
    console.log('--checking allowance')

    allowed = await getAllowance({
      owner: owner.contract as string,
      spender: spender.contract as string,
      signer,
      customAbi: customAbi ? customAbi : null,
    })
    await delay(100)
  }

  return allowed
}
