import { BigNumber, ethers } from 'ethers'
import { NFT_ABI, NFT_CONTRACT } from '../utils/constants'
import notificateTransaction from './shared/notificate-transaction'
import handleError from '../utils/handleErrors'
import { NFTAmount } from '../utils/enums'

export async function getNftInfos(signer: ethers.Signer) {
  try {
    const contract = new ethers.Contract(
      NFT_CONTRACT?.contract as string,
      NFT_ABI,
      signer,
    )

    const unitPrice = await contract.UNIT_PRICE_AUTOBOT()
    const threePrice = await contract.THREE_PRICE_AUTOBOT()
    const fivePrice = await contract.FIVE_PRICE_AUTOBOT()
    const max = Number(await contract.MAX_AUTOBOTS())
    const totalSupply = Number(await contract.totalSupply())

    const userBalance = Number(
      ethers.utils.formatEther(await signer.getBalance()),
    )

    return {
      unitPrice,
      threePrice,
      fivePrice,
      totalSupply,
      max,
      userBalance,
    }
  } catch (e) {
    handleError({ e: e, notificate: true })
  }
}

export async function listenToNewMints(signer: ethers.Signer, action: any) {
  const contract = new ethers.Contract(
    NFT_CONTRACT?.contract as string,
    NFT_ABI,
    signer,
  )
  contract.on('AutobotMinted', async () => {
    console.log('A NEW ENTER DETECTED!')
    action()
  })
}

export async function mint(
  nftAmount: NFTAmount,
  nftPrice: BigNumber,
  signer: ethers.Signer,
) {
  const network = await signer.provider?.getNetwork()

  try {
    const contract = new ethers.Contract(
      NFT_CONTRACT?.contract as string,
      NFT_ABI,
      signer,
    )

    let tx
    if (nftAmount === NFTAmount.one) {
      tx = await contract.mintOne({ value: nftPrice })
    } else if (nftAmount === NFTAmount.three) {
      tx = await contract.mintThree({ value: nftPrice })
    } else {
      tx = await contract.mintFive({ value: nftPrice })
    }
    await notificateTransaction(network as ethers.providers.Network, tx, 'Mint')
  } catch (e) {
    handleError({ e: e, notificate: true })
  }
}
